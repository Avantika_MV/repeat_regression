package com.mv.credit.repeat.utils;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public class JsonModificationsUtility {

    public static JSONObject modifyPayloadForChain(JSONObject jsonPayload, String chain, String source) throws IOException, ParseException {

        RandomGeneratorUtility rguObj = new RandomGeneratorUtility();
        String la_id = rguObj.generateLaId();
        String lan = rguObj.generateLan();
        JSONObject updatedPayload = JSONUtility.getPayloadAsString(jsonPayload, "loanApplicationInfo", "loanApplicationId", la_id);
        updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "loanApplicationNumber", lan);


        if (true) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "appName", "com.whizdm.moneyview.loans");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "channelName", "MVL_APP");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "lowGrowApplied", "false");

            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "isLgEligible", "false");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "isStarterRepeatEligible", "false");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "starterRepeatUtil", "false");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldLoanAmount", "18000");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldLoanEMI", "2500");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldLoanTenure", "9");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldDisbursalDate", "2020-01-01");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "daysSinceLastClosed", "29");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldRoi", "25");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldProcFee", "810");

            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "chainInfo", "productId", "6");
        }

        if (chain.contains("_LGRU")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "loanApplicationInfo", "lowGrowApplied", "true");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "isLgEligible", "true");
        }
        if (chain.contains("_STARTER_RU")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "isStarterRepeatEligible", "true");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "starterRepeatUtil", "true");
        }
        //if (!chain.contains("_LGRU") && !chain.contains("_STARTER_RU") && chain.contains("_RU")) {
        //}
        //if (!chain.contains("_LGRU") && !chain.contains("_STARTER_RU") && !chain.contains("_RU") && chain.contains("PC_")) {
        //}


        if (chain.contains("_PREMIUM_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "noOfLoanPayments", "6");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "maxDPD", "10");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "daysSinceLastClosed", "29");
        }
        if (chain.contains("_REGULAR_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "noOfLoanPayments", "3");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "maxDPD", "19");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "daysSinceLastClosed", "29");
        }
        if (!chain.contains("_LGRU") && !chain.contains("_STARTER_RU") && !chain.contains("_RU") && chain.contains("PC_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "noOfLoanPayments", "5");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "maxDPD", "20");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "daysSinceLastClosed", "90");
        }
        if (chain.contains("_DMI_STARTER_RU")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "noOfLoanPayments", "3");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "maxDPD", "0");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "daysSinceLastClosed", "29");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldLoanAmount", "10000");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldLoanEMI", "2267");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldLoanTenure", "6");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldDisbursalDate", "2020-01-01");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "daysSinceLastClosed", "29");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldRoi", "36");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldProcFee", "800");
        }
        if (chain.contains("_WHIZDM_STARTER_RU")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "noOfLoanPayments", "3");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "maxDPD", "0");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "daysSinceLastClosed", "29");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldLoanAmount", "4000");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldLoanEMI", "1854");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldLoanTenure", "3");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldDisbursalDate", "2020-01-01");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "daysSinceLastClosed", "29");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldRoi", "39");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldProcFee", "320");
        }


        if (chain.contains("_TERM-SE_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "employmentInfo", "employmentType", "Self-Employed");
        }
        if (chain.contains("_TERM-SAL_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "employmentInfo", "employmentType", "Salaried");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "employmentInfo", "employerName", "TOSHIBA TRANSMISSION & DISTRIBUTION SYSTEMS (INDIA) PRIVATE LIMITED");
        }

        if (chain.contains("_COVID") || chain.contains("_GROWTH_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "cashFlowInfo", "income", "23000");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "cashFlowInfo", "declaredIncome", "23000");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "cashFlowInfo", "opsCalculatedIncome", "23000");
        }
        if (!chain.contains("_COVID") && !chain.contains("_GROWTH_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "cashFlowInfo", "income", "43000");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "cashFlowInfo", "declaredIncome", "43000");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "cashFlowInfo", "opsCalculatedIncome", "43000");
        }

        if (chain.contains("_FICCL_") && !chain.contains("_LOC_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "pastPartnerId", "4");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldPartnerName", "FICCL");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "chainInfo", "partner", "4");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "fraudDetailsInfo", "dedupPartner", "4");
        }
        if (chain.contains("_FICCL_LOC_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "pastPartnerId", "6");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldPartnerName", "FICCL_LOC");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "chainInfo", "partner", "6");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "fraudDetailsInfo", "dedupPartner", "6");
        }
        if (chain.contains("_DMI_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "pastPartnerId", "3");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldPartnerName", "DMI");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "chainInfo", "partner", "3");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "fraudDetailsInfo", "dedupPartner", "3");
        }
        if (chain.contains("_WHIZDM_")) {
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "pastPartnerId", "6");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldPartnerName", "FICCL_LOC");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "chainInfo", "partner", "6");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "fraudDetailsInfo", "dedupPartner", "6");
            //updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "fraudDetailsInfo", "dedup", "FOUND");
            //updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "fraudDetailsInfo", "dmiRejectReason", "Hunter Match");
            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "employmentInfo", "employerName", "GIRIRAJ TEXTILE");

//            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "pastPartnerId", "7");
//            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "repeatUserDTO", "oldPartnerName", "WHIZDM");
//            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "chainInfo", "partner", "7");
//            updatedPayload = JSONUtility.getPayloadAsString(updatedPayload, "fraudDetailsInfo", "dedupPartner", "7");
        }



        // System.out.println(jsonObject);
        return updatedPayload;

    }

}
