package com.mv.credit.repeat.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class ReadfileUtility {

    static String fileName;

    private static final Logger _logger = LogManager.getLogger(ReadfileUtility.class.getName());


    @SuppressWarnings("static-access")
    public ReadfileUtility(String fileName) {
        super();
        this.fileName = fileName;
    }

    public String ReadFromFile() {
        String FILENAME = fileName;
        if (FILENAME.length() == 0) {
            _logger.warn("Filename is null");
            //FILENAME = "dummy.txt";
        }
        BufferedReader br = null;
        FileReader fr = null;
        String sCurrentLine = "";
        String textinfile = "";

        try {
            fr = new FileReader(FILENAME);
            br = new BufferedReader(fr);
            while ((sCurrentLine = br.readLine()) != null) {
                textinfile += sCurrentLine;
            }
            fr.close();
        } catch (IOException e) {
            _logger.error("Problem in reading file, exception " + e.getMessage());

        }
        _logger.info("File Read Success");
        return textinfile;

    }


}