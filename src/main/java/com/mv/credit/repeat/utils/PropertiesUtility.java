package com.mv.credit.repeat.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

// import org.apache.logging.log4j.LogManager;
// import org.apache.logging.log4j.Logger;

public class PropertiesUtility {

    //private static final Logger _logger = LogManager.getLogger(PropertiesUtility.class);

    protected static final String CURRENT_DIR = System.getProperty("user.dir");
    protected static final String FILE_SEPERATOR = System.getProperty("file.seperator");


    public static HashMap<String, String> readEnvironmentName (String filename, String keyString) throws IOException {
        String environment = "NewEnvironmentDetails";
        HashMap<String, String> myConfProperty = new HashMap<String, String>();

        if (filename != null) {
            HashMap<String, String> myConfProperty1 = new HashMap<String, String>();
            try {
                FileInputStream fis = new FileInputStream(CURRENT_DIR + "/properties/" + filename + ".properties");
                Properties prop = new Properties();
                prop.load(fis);
                for (String key : prop.stringPropertyNames()) {
                    String value = prop.getProperty(key);
                    myConfProperty1.put(key, value);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            environment = myConfProperty1.get(keyString);
        }
        myConfProperty = readProperty(environment);
        return myConfProperty;
    }

    public static HashMap<String, String> readProperty (String environment) throws IOException {
        HashMap<String, String> myConfProperty = new HashMap<String, String>();
        try {
            FileInputStream fis = new FileInputStream(CURRENT_DIR +"/properties/"+environment+".properties");
            Properties prop = new Properties();
            prop.load(fis);
            for (String key : prop.stringPropertyNames()) {
                String value = prop.getProperty(key);
                myConfProperty.put(key, value);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return myConfProperty;
    }

}
