package com.mv.credit.repeat.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DB_QueriesUtility {

    public static Object readAttributeFromTable (Connection conn, String tableName, String readAttribute, String searchAttribute, String searchValue) throws SQLException {
        String query = "SELECT "+readAttribute+" FROM "+tableName+" WHERE "+searchAttribute+" = \""+searchValue+"\"";
        ResultSet rs = DB_ConnectionsUtility.selectQueryResultSet(query, conn);
        return rs.getObject(readAttribute);
    }

}
