package com.mv.credit.repeat.base;

import com.mv.credit.repeat.utils.DB_ConnectionsUtility;
import com.mv.credit.repeat.utils.PropertiesUtility;
import com.mv.credit.repeat.utils.ReadfileUtility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

public class RepeatTestBase {

    private static final Logger _logger = LogManager.getLogger(RepeatTestBase.class.getName());
    public static HashMap<String, String> myConfProperty = new HashMap<String, String>();
    public static HashMap<String, String> myConfProperty1 = new HashMap<String, String>();
    public static Connection credit_conn;
    public static Connection data_conn;
    public static Connection lending_conn;
    public static Connection whiz_credit_conn;
    public static Connection whiz_data_conn;
    public static Connection whiz_lending_conn;

    public static final String insertQueryFileNames[] = {"credit_report_response_info_cibilV3_dmi","credit_report_response_info_experian","credit_report_response_info_cibilV3_ficcl_loc"};
    public static final String deleteQueryFileNames[] = {"delete_credit_report_account_info","delete_credit_report","delete_credit_report_response_info"};
    public static final String updateQueryFileNames[] = {"update_credit_report_response_info"};
    public static final String changeQueryFileNames[] = {"change_directive_parameter"};
    public static final String revertQueryFileNames[] = {"revert_directive_parameter"};

    protected static final String CURRENT_DIR = System.getProperty("user.dir");




    @BeforeSuite
    public void setupBeforeSuite() throws IOException, SQLException, ClassNotFoundException {
        _logger.info("Executing ::::  setupBeforeSuite");
        myConfProperty = PropertiesUtility.readEnvironmentName("EnvironmentSetup","test_environment");
        myConfProperty1 = PropertiesUtility.readEnvironmentName("EnvironmentSetup","production");

        credit_conn = null;
        data_conn = null;
        lending_conn = null;
        whiz_credit_conn = null;
        whiz_data_conn = null;
        whiz_lending_conn = null;

        String creditDB_host = myConfProperty.get("creditDB_host");
        String creditDB_username = myConfProperty.get("creditDB_username");
        String creditDB_password = myConfProperty.get("creditDB_password");
        _logger.info("Fetched Credentials for ::::  Credit DB");
        credit_conn = DB_ConnectionsUtility.getDBConnection(creditDB_host, creditDB_username, creditDB_password);

        String dataDB_host = myConfProperty.get("dataDB_host");
        String dataDB_username = myConfProperty.get("dataDB_username");
        String dataDB_password = myConfProperty.get("dataDB_password");
        _logger.info("Fetched Credentials for ::::  Data DB");
        data_conn = DB_ConnectionsUtility.getDBConnection(dataDB_host, dataDB_username, dataDB_password);

        String lendingDB_host = myConfProperty.get("lendingDB_host");
        String lendingDB_username = myConfProperty.get("lendingDB_username");
        String lendingDB_password = myConfProperty.get("lendingDB_password");
        _logger.info("Fetched Credentials for ::::  Lending DB");
        //lending_conn = DB_ConnectionsUtility.getDBConnection(lendingDB_host, lendingDB_username, lendingDB_password);

        String whizCreditDB_host = myConfProperty1.get("whizCreditDB_host");
        String whizCreditDB_username = myConfProperty1.get("whizCreditDB_username");
        String whizCreditDB_password = myConfProperty1.get("whizCreditDB_password");
        _logger.info("Fetched Credentials for ::::  Production Credit DB");
        //whiz_credit_conn = DB_ConnectionsUtility.getDBConnection(whizCreditDB_host, whizCreditDB_username, whizCreditDB_password);

        String whizDataDB_host = myConfProperty1.get("whizDataDB_host");
        String whizDataDB_username = myConfProperty1.get("whizDataDB_username");
        String whizDataDB_password = myConfProperty1.get("whizDataDB_password");
        _logger.info("Fetched Credentials for ::::  Production Data DB");
        //whiz_data_conn = DB_ConnectionsUtility.getDBConnection(whizDataDB_host, whizDataDB_username, whizDataDB_password);

        String whizLendingDB_host = myConfProperty1.get("whizLendingDB_host");
        String whizLendingDB_username = myConfProperty1.get("whizLendingDB_username");
        String whizLendingDB_password = myConfProperty1.get("whizLendingDB_password");
        _logger.info("Fetched Credentials for ::::  Production Lending DB");
        //whiz_lending_conn = DB_ConnectionsUtility.getDBConnection(whizLendingDB_host, whizLendingDB_username, whizLendingDB_password);

        // DELETE PAN DETAILS FROM TEST DATA DB
        for (String s : deleteQueryFileNames) {
            String queryFileName = PropertiesUtility.readProperty("Pan").get(s);
            ReadfileUtility fr = new ReadfileUtility(CURRENT_DIR +"/pan_setup_queries/"+queryFileName);
            String query = fr.ReadFromFile();
            String result = DB_ConnectionsUtility.updateQuery(query,data_conn);
            _logger.info("Query Result :::: "+result);

        }

        // INSERT PAN DETAILS IN TEST DATA DB
        for (String s : insertQueryFileNames) {
            String queryFileName = PropertiesUtility.readProperty("Pan").get(s);
            ReadfileUtility fr = new ReadfileUtility(CURRENT_DIR +"/pan_setup_queries/"+queryFileName);
            String query = fr.ReadFromFile();
            String result = DB_ConnectionsUtility.updateQuery(query,data_conn);
            _logger.info("Query Result :::: "+result);
        }

        // UPDATE PAN DETAILS IN TEST DATA DB
        for (String s : updateQueryFileNames) {
            String queryFileName = PropertiesUtility.readProperty("Pan").get(s);
            ReadfileUtility fr = new ReadfileUtility(CURRENT_DIR +"/pan_setup_queries/"+queryFileName);
            String query = fr.ReadFromFile();
            String result = DB_ConnectionsUtility.updateQuery(query,data_conn);
            _logger.info("Query Result :::: "+result);
        }

        // CHANGE DEDUP VALUE IN TEST CREDIT DB
        for (String s : changeQueryFileNames) {
            //String queryFileName = PropertiesUtility.readProperty("Pan").get(s);
            //ReadfileUtility fr = new ReadfileUtility(CURRENT_DIR +"/pan_setup_queries/"+queryFileName);
            //String query = fr.ReadFromFile();
            //String result = DB_ConnectionsUtility.updateQuery(query,credit_conn);
            //_logger.info("Query Result :::: "+result);
        }

    }

//    @BeforeClass
//    public void setupBeforeClass() throws IOException, SQLException, ClassNotFoundException {
//        _logger.info("Executing ::::  setupBeforeClass");
//        myConfProperty = PropertiesUtility.readEnvironmentName("EnvironmentSetup","test_environment");
//        myConfProperty1 = PropertiesUtility.readEnvironmentName("EnvironmentSetup","production");
//
//        credit_conn = null;
//        data_conn = null;
//        lending_conn = null;
//        whiz_credit_conn = null;
//        whiz_data_conn = null;
//        whiz_lending_conn = null;
//
//        String creditDB_host = myConfProperty.get("creditDB_host");
//        String creditDB_username = myConfProperty.get("creditDB_username");
//        String creditDB_password = myConfProperty.get("creditDB_password");
//        _logger.info("Fetched Credentials for ::::  Credit DB");
//        credit_conn = DB_ConnectionsUtility.getDBConnection(creditDB_host, creditDB_username, creditDB_password);
//
//        String dataDB_host = myConfProperty.get("dataDB_host");
//        String dataDB_username = myConfProperty.get("dataDB_username");
//        String dataDB_password = myConfProperty.get("dataDB_password");
//        _logger.info("Fetched Credentials for ::::  Data DB");
//        data_conn = DB_ConnectionsUtility.getDBConnection(dataDB_host, dataDB_username, dataDB_password);
//
//        String lendingDB_host = myConfProperty.get("lendingDB_host");
//        String lendingDB_username = myConfProperty.get("lendingDB_username");
//        String lendingDB_password = myConfProperty.get("lendingDB_password");
//        _logger.info("Fetched Credentials for ::::  Lending DB");
//        //lending_conn = DB_ConnectionsUtility.getDBConnection(lendingDB_host, lendingDB_username, lendingDB_password);
//
//        String whizCreditDB_host = myConfProperty1.get("whizCreditDB_host");
//        String whizCreditDB_username = myConfProperty1.get("whizCreditDB_username");
//        String whizCreditDB_password = myConfProperty1.get("whizCreditDB_password");
//        _logger.info("Fetched Credentials for ::::  Production Credit DB");
//        //whiz_credit_conn = DB_ConnectionsUtility.getDBConnection(whizCreditDB_host, whizCreditDB_username, whizCreditDB_password);
//
//        String whizDataDB_host = myConfProperty1.get("whizDataDB_host");
//        String whizDataDB_username = myConfProperty1.get("whizDataDB_username");
//        String whizDataDB_password = myConfProperty1.get("whizDataDB_password");
//        _logger.info("Fetched Credentials for ::::  Production Data DB");
//        //whiz_data_conn = DB_ConnectionsUtility.getDBConnection(whizDataDB_host, whizDataDB_username, whizDataDB_password);
//
//        String whizLendingDB_host = myConfProperty1.get("whizLendingDB_host");
//        String whizLendingDB_username = myConfProperty1.get("whizLendingDB_username");
//        String whizLendingDB_password = myConfProperty1.get("whizLendingDB_password");
//        _logger.info("Fetched Credentials for ::::  Production Lending DB");
//        //whiz_lending_conn = DB_ConnectionsUtility.getDBConnection(whizLendingDB_host, whizLendingDB_username, whizLendingDB_password);
//
//        // DELETE PAN DETAILS FROM TEST DATA DB
//        for (String s : deleteQueryFileNames) {
//            String queryFileName = PropertiesUtility.readProperty("Pan").get(s);
//            ReadfileUtility fr = new ReadfileUtility(CURRENT_DIR +"/pan_setup_queries/"+queryFileName);
//            String query = fr.ReadFromFile();
//            String result = DB_ConnectionsUtility.updateQuery(query,data_conn);
//            _logger.info("Query Result :::: "+result);
//
//        }
//
//        // INSERT PAN DETAILS IN TEST DATA DB
//        for (String s : insertQueryFileNames) {
//            String queryFileName = PropertiesUtility.readProperty("Pan").get(s);
//            ReadfileUtility fr = new ReadfileUtility(CURRENT_DIR +"/pan_setup_queries/"+queryFileName);
//            String query = fr.ReadFromFile();
//            String result = DB_ConnectionsUtility.updateQuery(query,data_conn);
//            _logger.info("Query Result :::: "+result);
//        }
//
//        // UPDATE PAN DETAILS IN TEST DATA DB
//        for (String s : updateQueryFileNames) {
//            String queryFileName = PropertiesUtility.readProperty("Pan").get(s);
//            ReadfileUtility fr = new ReadfileUtility(CURRENT_DIR +"/pan_setup_queries/"+queryFileName);
//            String query = fr.ReadFromFile();
//            String result = DB_ConnectionsUtility.updateQuery(query,data_conn);
//            _logger.info("Query Result :::: "+result);
//        }
//    }

//    @BeforeTest

//    @AfterTest

//    @AfterClass
//    public void cleanupAfterClass() throws SQLException, NullPointerException, IOException {
//        _logger.info("Executing ::::  cleanupAfterClass");
//
//        // DELETE PAN DETAILS FROM TEST DATA DB
//        for (String s : deleteQueryFileNames) {
//            String queryFileName = PropertiesUtility.readProperty("Pan").get(s);
//            ReadfileUtility fr = new ReadfileUtility(CURRENT_DIR +"/pan_setup_queries/"+queryFileName);
//            String query = fr.ReadFromFile();
//            String result = DB_ConnectionsUtility.updateQuery(query,data_conn);
//            _logger.info("Query Result :::: "+result);
//        }
//
//        // CLOSE DB CONNECTIONS
//        if (credit_conn != null)
//            credit_conn.close();
//        if (data_conn != null)
//            data_conn.close();
//        if (lending_conn != null)
//            lending_conn.close();
//        if (whiz_data_conn != null)
//            whiz_data_conn.close();
//    }

    @AfterSuite
    public void cleanupAfterSuite() throws SQLException, NullPointerException, IOException {
        _logger.info("Executing ::::  cleanupAfterSuite");

        // DELETE PAN DETAILS FROM TEST DATA DB
//        for (String s : deleteQueryFileNames) {
//            String queryFileName = PropertiesUtility.readProperty("Pan").get(s);
//            ReadfileUtility fr = new ReadfileUtility(CURRENT_DIR +"/pan_setup_queries/"+queryFileName);
//            String query = fr.ReadFromFile();
//            String result = DB_ConnectionsUtility.updateQuery(query,data_conn);
//            _logger.info("Query Result :::: "+result);
//        }

        // REVERT DEDUP VALUE IN TEST CREDIT DB
        for (String s : revertQueryFileNames) {
            //String queryFileName = PropertiesUtility.readProperty("Pan").get(s);
            //ReadfileUtility fr = new ReadfileUtility(CURRENT_DIR +"/pan_setup_queries/"+queryFileName);
            //String query = fr.ReadFromFile();
            //String result = DB_ConnectionsUtility.updateQuery(query,credit_conn);
            //_logger.info("Query Result :::: "+result);
        }

        // CLOSE DB CONNECTIONS
        if (credit_conn != null)
            credit_conn.close();
        if (data_conn != null)
            data_conn.close();
        if (lending_conn != null)
            lending_conn.close();
        if (whiz_data_conn != null)
            whiz_data_conn.close();
    }

}
