package com.mv.credit.repeat.base;

import io.restassured.path.json.JsonPath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RepeatOldOfferBase {

    private static final Logger _logger = LogManager.getLogger(RepeatOldOfferBase.class.getName());

    public static int get_oldLoanInterestRate(String payloadString) {

        int oldLoanInterestRate = JsonPath.from(payloadString).getInt("repeatUserDTO.oldRoi");
        _logger.info("oldLoanInterestRate ::: " + oldLoanInterestRate);
        return oldLoanInterestRate;

    }

    public static int get_oldLoanEMI(String payloadString) {

        int oldLoanEMI = JsonPath.from(payloadString).getInt("repeatUserDTO.oldLoanEMI");
        _logger.info("oldLoanEMI ::: " + oldLoanEMI);
        return oldLoanEMI;

    }

    public static int get_oldLoanTenure(String payloadString) {

        int oldLoanTenure = JsonPath.from(payloadString).getInt("repeatUserDTO.oldLoanTenure");
        _logger.info("oldLoanTenure ::: " + oldLoanTenure);
        return oldLoanTenure;

    }

    public static int get_oldLoanAmount(String payloadString) {

        int oldLoanAmount = JsonPath.from(payloadString).getInt("repeatUserDTO.oldLoanAmount");
        _logger.info("oldLoanAmount ::: " + oldLoanAmount);
        return oldLoanAmount;

    }

    public static int get_oldLoanPartnerId(String payloadString) {

        int oldLoanPartnerId = JsonPath.from(payloadString).getInt("repeatUserDTO.pastPartnerId");
        _logger.info("oldLoanPartnerId ::: " + oldLoanPartnerId);
        return oldLoanPartnerId;

    }

}
