package com.mv.credit.repeat.base;

import com.mv.credit.repeat.utils.DB_QueriesUtility;
import io.restassured.path.json.JsonPath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import java.sql.SQLException;

public class RepeatOfferValidationsBase extends RepeatTestBase {

    private static final Logger _logger = LogManager.getLogger(RepeatOfferValidationsBase.class.getName());


    public static String validate_chainName(String responseBody, String testChain) {

        String chainName = JsonPath.from(responseBody).getString("body.chainInfoResponse.chainName");
        _logger.info("chainName ::: " + chainName);
        Assert.assertTrue(chainName.equalsIgnoreCase(testChain));
        return chainName;

    }

    public static void validate_offerFound(String responseBody, String chainName) {

        String offerMessage = JsonPath.from(responseBody).getString("body.offerResponse.message");
        _logger.info("offerMessage ::: " + offerMessage);
        Assert.assertTrue(offerMessage.equalsIgnoreCase("Offers found"));

        Boolean offerAvailable = JsonPath.from(responseBody).getBoolean("body.offerResponse.offerAvailable");
        _logger.info("offerAvailable ::: " + offerAvailable);
        Assert.assertTrue(offerAvailable);

        String message = JsonPath.from(responseBody).getString("message");
        _logger.info("message ::: " + message);
        Assert.assertTrue(message.equalsIgnoreCase("Offer Assigned"));

    }

    public static void validate_offerAmount(String responseBody, String chainName) throws SQLException {

        double loanAmount = JsonPath.from(responseBody).getDouble("body.offerResponse.bestOffer.loanAmount");
        _logger.info("loanAmount ::: " + loanAmount);
        //double repeatMin = 5000.0; // read from directive as per chainName
        double repeatMin = (double) DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","loan_amount_floor","chain_name",chainName);
        //double repeatMax = 500000.0; // read from directive as per chainName
        double repeatMax = (double) DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","loan_amount_ceil","chain_name",chainName);
        Assert.assertTrue(loanAmount >= repeatMin && loanAmount <= repeatMax);

    }

    public static void validate_offerProductId(String responseBody, String chainName) throws SQLException {

        String loanProductId = JsonPath.from(responseBody).getString("body.offerResponse.bestOffer.loanProductId");
        _logger.info("loanProductId ::: " + loanProductId);
        // read from directive as per chainName
        String product_id = DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","product_id","chain_name",chainName).toString();
        Assert.assertTrue(loanProductId.equalsIgnoreCase(product_id));

    }

    public static void validate_offerTenure(String responseBody, String chainName) throws SQLException {

        int loanTenure = JsonPath.from(responseBody).getInt("body.offerResponse.bestOffer.loanTenure");
        _logger.info("loanTenure ::: " + loanTenure);
        //int repeatMin = 6; // read from directive as per chainName
        int repeatMin = (int) DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","loan_tenure_floor","chain_name",chainName);
        //int repeatMax = 60; // read from directive as per chainName
        int repeatMax = (int) DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","loan_tenure_ceil","chain_name",chainName);
        Assert.assertTrue(loanTenure >= repeatMin && loanTenure <= repeatMax);

    }

    public static void validate_offerROI(String responseBody, String chainName) throws SQLException {

        double interestRate = JsonPath.from(responseBody).getDouble("body.offerResponse.bestOffer.interestRate");
        _logger.info("interestRate ::: " + interestRate);
        //double repeatMin = 15.0; // read from directive as per chainName
        double repeatMin = (double) (int) DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","interest_rate_floor","chain_name",chainName);
        //double repeatMax = 39.0; // read from directive as per chainName
        double repeatMax = (double) DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","interest_rate_ceil","chain_name",chainName);
        Assert.assertTrue(interestRate >= repeatMin && interestRate <= repeatMax);

    }

    public static void validate_offerMaxEMIWithRespectToOldEMI(String responseBody, String chainName, String source, int oldLoanEMI) {

        double newBestOfferEmi = JsonPath.from(responseBody).getDouble("body.offerResponse.bestOffer.emi");
        _logger.info("newBestOfferEmi ::: " + newBestOfferEmi);
        double emiCap = 1.0; // TO-DO read from directive as per repeat type
        if (chainName.contains("_STARTER_RU"))
            emiCap = 1.2; // TO-DO read from directive as per repeat type
        else if (chainName.contains("_PREMIUM_LGRU"))
            emiCap = 1.25; // TO-DO read from directive as per repeat type
        else if (chainName.contains("_PREMIUM_RU"))
            emiCap = 1.5; // TO-DO read from directive as per repeat type
        _logger.info("required emiCapping ::: " + emiCap);
        Assert.assertTrue(newBestOfferEmi <= emiCap * oldLoanEMI);

    }

    public static void validate_pentileUpshift(String responseBody, String chainName) {

        int pentile = JsonPath.from(responseBody).getInt("body.transactionData.pentile");
        _logger.info("pentile ::: " + pentile);
        int bureauSeptile = JsonPath.from(responseBody).getInt("body.transactionData.bureauSeptile");
        _logger.info("bureauSeptile ::: " + bureauSeptile);
        if (chainName.contains("_PREMIUM_RU") || chainName.contains("_PREMIUM_LGRU")) { // premium RU , premium LGRU -> pentile = septile - 1
            if (bureauSeptile > 1 && bureauSeptile < 7)
                Assert.assertTrue(pentile == bureauSeptile - 1); // As per Business
            else if (bureauSeptile == 1 || bureauSeptile == 7)
                Assert.assertTrue(pentile == bureauSeptile); // As per Business
            else
                _logger.error("BUREAU SEPTILE IS NEGATIVE");
        }
        if (chainName.contains("_STARTER_RU") || chainName.contains("_REGULAR_RU") || chainName.contains("_REGULAR_LGRU")) { // regular RU , stater RU , regular LGRU -> pentile = septile
            if (bureauSeptile > 0 && bureauSeptile < 8)
                Assert.assertTrue(pentile == bureauSeptile); // As per Business
            else
                _logger.error("BUREAU SEPTILE IS NEGATIVE");
        }

    }

    public static void validate_primaryBureau(String responseBody, String chainName) throws SQLException {

        String creditReportProvider = JsonPath.from(responseBody).getString("body.transactionData.creditReportProvider");
        _logger.info("creditReportProvider ::: " + creditReportProvider);
        String primaryBureau = DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","primary_bureau","chain_name",chainName).toString();
        if (creditReportProvider!=null)
            Assert.assertTrue(creditReportProvider.equals(primaryBureau)); // As per Business
        else
            _logger.error("NO PRIMARY BUREAU CREDIT REPORT PULLED");

    }

    public static void validate_obligationDeduction(String responseBody, String chainName) throws SQLException {

        double obligations = JsonPath.from(responseBody).getDouble("body.transactionData.obligations");
        _logger.info("newObligations ::: " + obligations);
        String creditReportId = JsonPath.from(responseBody).getString("body.transactionData.creditReportId");
        _logger.info("creditReportId ::: " + creditReportId);
        if (!(creditReportId.isEmpty() || creditReportId.equalsIgnoreCase("null"))) {
            double oldObligations = (double) DB_QueriesUtility.readAttributeFromTable(data_conn, "credit_report", "total_obligations", "id", creditReportId);
            if (obligations >= 0.0)
                Assert.assertTrue(obligations <= oldObligations - 500.0); // As per Business
            else
                _logger.error("NEGATIVE NEW OBLIGATIONS");
        }
        else
            _logger.error("NO BUREAU CREDIT REPORT PULLED");

    }

    public static void validate_offerProcFeeRate(String responseBody, String chainName) throws SQLException {

        double loanAmount = JsonPath.from(responseBody).getDouble("body.offerResponse.bestOffer.loanAmount");
        _logger.info("loanAmount ::: " + loanAmount);
        double processingFeeAmount = JsonPath.from(responseBody).getDouble("body.offerResponse.bestOffer.processingFeeAmount");
        _logger.info("processingFeeAmount ::: " + processingFeeAmount);
        double newProcFeeRate = processingFeeAmount/loanAmount * 100.0;
        _logger.info("newProcFeeRate ::: " + newProcFeeRate);
        double proc_charge_floor = 2.0; // As per Business
        //double proc_charge_ceil = 7.0; // read from directive as per chainName
        double proc_charge_ceil = (double) DB_QueriesUtility.readAttributeFromTable(credit_conn,"chain_definition","proc_charge_ceil","chain_name",chainName);
        Assert.assertTrue(newProcFeeRate >= proc_charge_floor && newProcFeeRate <= proc_charge_ceil);

    }

    public static void validate_offerValid(String responseBody, String chainName) {

        String offerMessage = JsonPath.from(responseBody).getString("body.offerResponse.message");
        _logger.info("offerMessage ::: " + offerMessage);
        Assert.assertTrue(offerMessage.equalsIgnoreCase("Current Offer is valid"));

        Boolean offerAvailable = JsonPath.from(responseBody).getBoolean("body.offerResponse.offerAvailable");
        _logger.info("offerAvailable ::: " + offerAvailable);
        Assert.assertTrue(offerAvailable);

        String message = JsonPath.from(responseBody).getString("message");
        _logger.info("message ::: " + message);
        Assert.assertTrue(message.equalsIgnoreCase("Offer Assigned"));

    }

}
