package com.mv.credit.repeat.test;

import com.mv.credit.repeat.base.RepeatOfferValidationsBase;
import com.mv.credit.repeat.base.RepeatOldOfferBase;
import com.mv.credit.repeat.utils.JSONUtility;
import com.mv.credit.repeat.utils.PropertiesUtility;
import com.mv.credit.repeat.utils.REST_APIUtility;
import groovy.json.JsonSlurper;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.Assert;

import java.io.IOException;
import java.sql.SQLException;

public class ExecuteTest extends RepeatOfferValidationsBase {

    private static final Logger _logger = LogManager.getLogger(ExecuteTest.class.getName());


    public static void executeOfferGenerationTest(String testChain, String source) throws IOException, ParseException, SQLException {

        _logger.info("\n\nSTARTING OFFER GENERATION TEST EXECUTION FOR REPEAT ON SOURCE TYPE  ::::  "+source);

        // if chain is enabled
        // TO DO--------------------check from ChainName.properties if chain / user type is enabled
        if (PropertiesUtility.readProperty("ChainName").get(testChain).equals("1")) {

            // capture start time
            Long startTime = System.currentTimeMillis();
            System.out.println("\n------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            _logger.info(testChain+ " -------------------------------------- Test Execution Started at ::::  " + startTime + " ms");

            // read jsonPayload
            JSONObject jsonPayload = null;
            jsonPayload = JSONUtility.readPayload("getOffers_payload");
            // modify jsonPayload as per chain
            jsonPayload = JSONUtility.modifyPayload(jsonPayload, testChain, source);
            // convert jsonPayload to String
            String payloadString = JSONUtility.toJsonString(jsonPayload);
            // capture Payload
            _logger.info("Payload ::::  " + payloadString);

            // read url
            String url = myConfProperty.get("getOffers_api");

            // POST call
            Response response = REST_APIUtility.postCall(url, payloadString);

            // capture Response statusCode
            int statusCode = response.getStatusCode();
            _logger.info("Status code ::::  " + statusCode);
            Assert.assertEquals(statusCode, 200);

            // convert Response to String
            //String responseString = response.prettyPrint();
            String responseString = response.getBody().asString();
            // convert Response to JSON String
            String jsonResponse = JSONUtility.toJsonString(new JsonSlurper().parseText(responseString));
            if (jsonResponse.startsWith("\"") && jsonResponse.endsWith("\""))
                jsonResponse = jsonResponse.substring(1,jsonResponse.lastIndexOf('\"'));
            // capture Response
            _logger.info("Response ::::  " + jsonResponse);

            // read values from payload for old offer details
            System.out.println("---------------- read values from payload for old offer details ----------------");
            int oldLoanInterestRate = RepeatOldOfferBase.get_oldLoanInterestRate(payloadString);
            int oldLoanAmount = RepeatOldOfferBase.get_oldLoanAmount(payloadString);
            int oldLoanTenure = RepeatOldOfferBase.get_oldLoanTenure(payloadString);
            int oldLoanEMI = RepeatOldOfferBase.get_oldLoanEMI(payloadString);
            int oldLoanPartnerId = RepeatOldOfferBase.get_oldLoanPartnerId(payloadString);

            // call validation methods
            System.out.println("---------------- perform validations ----------------");
            String chainName = validate_chainName(jsonResponse, testChain);
            validate_offerFound(jsonResponse, chainName);
            validate_offerAmount(jsonResponse, chainName);
            validate_offerProductId(jsonResponse, chainName);
            validate_offerTenure(jsonResponse, chainName);
            validate_offerROI(jsonResponse, chainName);
            if (chainName.contains("_LGRU") || chainName.contains("_STARTER_RU") || (!chainName.contains("_LGRU") && !chainName.contains("_STARTER_RU") && !chainName.contains("_RU")))
                validate_offerMaxEMIWithRespectToOldEMI(jsonResponse, chainName, source, oldLoanEMI); // EMI Capping for Premium RU and Regular RU is not done in credit-side at present.
            validate_pentileUpshift(jsonResponse, chainName);
            validate_primaryBureau(jsonResponse, chainName);
            validate_obligationDeduction(jsonResponse, chainName);
            validate_offerProcFeeRate(jsonResponse, chainName);

            // capture end time
            Long endTime = System.currentTimeMillis();
            _logger.info(testChain+ " -------------------------------------- Test Execution Completed at ::::  " + endTime + " ms");

            Long testExecutionTime = endTime - startTime;
            _logger.info("Test Execution Time Taken ::::  " + testExecutionTime + "ms \n\n\n");
            System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");

        }
        else {
            System.out.println("\n------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            _logger.warn("Test Case not found enabled for  ::::  "+testChain);
            System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
        }

        _logger.info("ENDING OFFER GENERATION TEST EXECUTION FOR REPEAT ON SOURCE TYPE  ::::  "+source+"\n\n");

    }

    public static void executeOfferValidationTest(String testChain, String source) throws IOException, ParseException, SQLException {

        _logger.info("\n\nSTARTING OFFER VALIDATION TEST EXECUTION FOR REPEAT ON SOURCE TYPE  ::::  "+source);

        // if chain is enabled
        // TO DO--------------------check from ChainName.properties if chain / user type is enabled
        if (PropertiesUtility.readProperty("ChainName").get(testChain).equals("1")) {

            // capture start time
            Long startTime = System.currentTimeMillis();
            System.out.println("\n------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            _logger.info(testChain+ " -------------------------------------- Test Execution Started at ::::  " + startTime + " ms");

            // read jsonPayload
            JSONObject jsonPayload = null;
            jsonPayload = JSONUtility.readPayload("validateOffer_payload");
            // modify jsonPayload as per chain
            jsonPayload = JSONUtility.modifyPayload(jsonPayload, testChain, source);
            // convert jsonPayload to String
            String payloadString = JSONUtility.toJsonString(jsonPayload);
            // capture Payload
            _logger.info("Payload ::::  " + payloadString);

            // read url
            String url = myConfProperty.get("validateOffer_api");

            // POST call
            Response response = REST_APIUtility.postCall(url, payloadString);

            // capture Response statusCode
            int statusCode = response.getStatusCode();
            _logger.info("Status code ::::  " + statusCode);
            Assert.assertEquals(statusCode, 200);

            // convert Response to String
            //String responseString = response.prettyPrint();
            String responseString = response.getBody().asString();
            // convert Response to JSON String
            String jsonResponse = JSONUtility.toJsonString(new JsonSlurper().parseText(responseString));
            if (jsonResponse.startsWith("\"") && jsonResponse.endsWith("\""))
                jsonResponse = jsonResponse.substring(1,jsonResponse.lastIndexOf('\"'));
            // capture Response
            _logger.info("Response ::::  " + jsonResponse);

            // read values from payload for old offer details
            System.out.println("---------------- read values from payload for old offer details ----------------");
            int oldLoanInterestRate = RepeatOldOfferBase.get_oldLoanInterestRate(payloadString);
            int oldLoanAmount = RepeatOldOfferBase.get_oldLoanAmount(payloadString);
            int oldLoanTenure = RepeatOldOfferBase.get_oldLoanTenure(payloadString);
            int oldLoanEMI = RepeatOldOfferBase.get_oldLoanEMI(payloadString);
            int oldLoanPartnerId = RepeatOldOfferBase.get_oldLoanPartnerId(payloadString);

            // call validation methods
            System.out.println("---------------- perform validations ----------------");
            String chainName = validate_chainName(jsonResponse, testChain);
            validate_offerValid(jsonResponse, chainName);
            validate_offerAmount(jsonResponse, chainName);
            validate_offerProductId(jsonResponse, chainName);
            validate_offerTenure(jsonResponse, chainName);
            validate_offerROI(jsonResponse, chainName);
            if (chainName.contains("_LGRU") || chainName.contains("_STARTER_RU") || (!chainName.contains("_LGRU") && !chainName.contains("_STARTER_RU") && !chainName.contains("_RU")))
                validate_offerMaxEMIWithRespectToOldEMI(jsonResponse, chainName, source, oldLoanEMI); // EMI Capping for Premium RU and Regular RU is not done in credit-side at present.
            validate_pentileUpshift(jsonResponse, chainName);
            validate_primaryBureau(jsonResponse, chainName);
            validate_obligationDeduction(jsonResponse, chainName);
            validate_offerProcFeeRate(jsonResponse, chainName);

            // capture end time
            Long endTime = System.currentTimeMillis();
            _logger.info(testChain+ " -------------------------------------- Test Execution Completed at ::::  " + endTime + " ms");

            Long testExecutionTime = endTime - startTime;
            _logger.info("Test Execution Time Taken ::::  " + testExecutionTime + "ms \n\n\n");
            System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");

        }
        else {
            System.out.println("\n------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            _logger.warn("Test Case not found enabled for  ::::  "+testChain);
            System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
        }

        _logger.info("ENDING OFFER VALIDATION TEST EXECUTION FOR REPEAT ON SOURCE TYPE  ::::  "+source+"\n\n");

    }


}
