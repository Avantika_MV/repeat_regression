package com.mv.credit.repeat.test;

import com.mv.credit.repeat.ConstantData;
import com.mv.credit.repeat.base.*;
import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;

public class RepeatTestEntryClass extends RepeatOfferValidationsBase {

    public static final String source = "Regular";

    @Test (priority = 1)
    public void offerGeneration_SAL_STANDARD_DMI_PREMIUM_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_STANDARD_DMI_PREMIUM_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 2)
    public void offerGeneration_SE_STANDARD_DMI_PREMIUM_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SE_STANDARD_DMI_PREMIUM_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 3)
    public void offerGeneration_SAL_STANDARD_DMI_REGULAR_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_STANDARD_DMI_REGULAR_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 4)
    public void offerGeneration_SE_STANDARD_DMI_REGULAR_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SE_STANDARD_DMI_REGULAR_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 5)
    public void offerGeneration_SAL_GROWTH_DMI_COVID_PREMIUM_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_GROWTH_DMI_COVID_PREMIUM_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 6)
    public void offerGeneration_SE_GROWTH_DMI_COVID_PREMIUM_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SE_GROWTH_DMI_COVID_PREMIUM_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 7)
    public void offerGeneration_SAL_GROWTH_DMI_COVID_REGULAR_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_GROWTH_DMI_COVID_REGULAR_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 8)
    public void offerGeneration_SE_GROWTH_DMI_COVID_REGULAR_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SE_GROWTH_DMI_COVID_REGULAR_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 9)
    public void offerGeneration_SAL_GROWTH_DMI_PREMIUM_LGRU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_GROWTH_DMI_PREMIUM_LGRU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 10)
    public void offerGeneration_SE_GROWTH_DMI_PREMIUM_LGRU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SE_GROWTH_DMI_PREMIUM_LGRU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 11)
    public void offerGeneration_SAL_GROWTH_DMI_REGULAR_LGRU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_GROWTH_DMI_REGULAR_LGRU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 12)
    public void offerGeneration_SE_GROWTH_DMI_REGULAR_LGRU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SE_GROWTH_DMI_REGULAR_LGRU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 13)
    public void offerGeneration_SAL_GROWTH_DMI_STARTER_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_GROWTH_DMI_STARTER_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 14)
    public void offerGeneration_SE_GROWTH_DMI_STARTER_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SE_GROWTH_DMI_STARTER_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 15)
    public void offerGeneration_SAL_GROWTH_DMI_PREMIUM_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_GROWTH_DMI_PREMIUM_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 16)
    public void offerGeneration_SE_GROWTH_DMI_PREMIUM_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SE_GROWTH_DMI_PREMIUM_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 17)
    public void offerGeneration_SAL_GROWTH_DMI_REGULAR_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_GROWTH_DMI_REGULAR_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 18)
    public void offerGeneration_SE_GROWTH_DMI_REGULAR_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SE_GROWTH_DMI_REGULAR_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 19)
    public void offerGeneration_SAL_STANDARD_FICCL_PREMIUM_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_STANDARD_FICCL_PREMIUM_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 20)
    public void offerGeneration_SAL_STANDARD_FICCL_REGULAR_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_STANDARD_FICCL_REGULAR_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 21)
    public void offerGeneration_SAL_STANDARD_FICCL_LOC_PREMIUM_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_STANDARD_FICCL_LOC_PREMIUM_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 22)
    public void offerGeneration_SAL_STANDARD_FICCL_LOC_REGULAR_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_STANDARD_FICCL_LOC_REGULAR_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 23)
    public void offerGeneration_SAL_GROWTH_FICCL_LOC_COVID_PREMIUM_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_GROWTH_FICCL_LOC_COVID_PREMIUM_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 24)
    public void offerGeneration_SAL_GROWTH_FICCL_LOC_COVID_REGULAR_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_GROWTH_FICCL_LOC_COVID_REGULAR_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 25)
    public void offerGeneration_SAL_STANDARD_WHIZDM_PREMIUM_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_STANDARD_WHIZDM_PREMIUM_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 26)
    public void offerGeneration_SAL_STANDARD_WHIZDM_REGULAR_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_STANDARD_WHIZDM_REGULAR_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 27)
    public void offerGeneration_SAL_GROWTH_WHIZDM_COVID_PREMIUM_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_GROWTH_WHIZDM_COVID_PREMIUM_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 28)
    public void offerGeneration_SAL_GROWTH_WHIZDM_COVID_REGULAR_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SAL_GROWTH_WHIZDM_COVID_REGULAR_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 29)
    public void offerGeneration_SE_STANDARD_WHIZDM_PREMIUM_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SE_STANDARD_WHIZDM_PREMIUM_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 30)
    public void offerGeneration_SE_STANDARD_WHIZDM_REGULAR_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SE_STANDARD_WHIZDM_REGULAR_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 31)
    public void offerGeneration_SE_GROWTH_WHIZDM_COVID_PREMIUM_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SE_GROWTH_WHIZDM_COVID_PREMIUM_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

    @Test (priority = 32)
    public void offerGeneration_SE_GROWTH_WHIZDM_COVID_REGULAR_RU() throws IOException, ParseException, SQLException {
        String chainStr = ConstantData.SE_GROWTH_WHIZDM_COVID_REGULAR_RU;
        ExecuteTest.executeOfferGenerationTest(chainStr,source);
    }

}
